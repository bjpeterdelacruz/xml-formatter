# About

This repository contains a Java program that will
output a properly formatted XML file.

## Prerequisites

[Java 8 SDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) must be installed.

## Usage

First, compile the program:

**javac XmlFormatter.java**

Then run it from the command line:

**java XmlFormatter /path/to/input.xml path/to/output.xml**

