import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;

/**
 * This class contains a method that will return a properly formatted XML string that can be read
 * easily by human eyes.
 * 
 * @author BJ Peter DeLaCruz
 */
public class XmlFormatter {

  /**
   * Returns a formatted XML string.
   * 
   * @param xml An unformatted XML string.
   * @return A formatted XML string.
   */
  public static String format(String xml) {
    try {
      InputSource src = new InputSource(new StringReader(xml));
      Node document =
          DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(src).getDocumentElement();
      Boolean keepDeclaration = Boolean.valueOf(xml.startsWith("<?xml"));

      DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
      DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
      LSSerializer writer = impl.createLSSerializer();

      writer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE);
      writer.getDomConfig().setParameter("xml-declaration", keepDeclaration);

      return writer.writeToString(document);
    }
    catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Main method.
   * 
   * @param args First argument is a valid path to an XML file.
   */
  public static void main(String[] args) {
    if (args.length != 2) {
      String help = "First argument must be a path to a valid input file, ";
      help += "e.g. /Users/johndoe/Documents/input.xml\n";
      help += "Second argument must be a path to an output file, ";
      help += "e.g. /Users/johndoe/Documents/output.xml";
      System.out.println(help);
      return;
    }
    List<String> lines;
    try {
      lines = Files.readAllLines(Paths.get(args[0]));
    }
    catch (IOException e) {
      e.printStackTrace();
      return;
    }
    StringBuffer buffer = new StringBuffer();
    for (String line : lines) {
      buffer.append(line);
    }
    try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(args[1]))) {
      writer.write(XmlFormatter.format(buffer.toString()));
      System.out.println("Successfully wrote formatted XML to output file: " + args[1]);
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }
}